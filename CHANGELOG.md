# Version Log #

### v1.0.1 - Mar 11 2020

* Added ability to override template from notifications service

### v1.0.2 - Aug 17 2020

* Fixed ability to override template from notifications service
* Added new configuration parameter (templatePath) to determine the path of a template in the notification service (Default: /app/mails) 