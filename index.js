var _        = require("lodash");
var nunjucks = require("nunjucks");
var fs = require("fs");

/**
 * Credentials
 * apiKey and domain
 *
 */

module.exports = function mailgunHook(config){
  var mailgun = require("mailgun-js")(config.credentials);

  function sendFn(params, cb){

    var required = ['templateName', 'templateContent']
    var emptyRows = [];

    if(_.isEmpty(params))
      return cb("The parameters mustn\'t be empty");

    required.forEach(function(key){
      if(!params[key]) emptyRows.push(key)
    })

    if(!_.isEmpty(emptyRows))
      return cb(`${emptyRows.toString()} can't be empty`);

    sendMail(params, (err, response)=>{
      if(err) return cb(err);

      cb(null, response)
    })

  }

  function buildMessage (params){

    var message;

    if(params.useTemplateName){
      message = {
        from: fromMail(),
        to: params.to,
        subject: params.subject,
        template: params.templateName,
        'h:X-Mailgun-Variables': params.templateVars
      }
    }else{
      message = {
        from: fromMail(),
        to: params.to,
        subject: params.subject,
        html: createTemplate(
          params.templateName,
          params.templateContent,
          params.templateVars
        )
      }

    }


    if(!_.isEmpty(params.attachment)){
      message.attachment = new mailgun.Attachment({
        data: new Buffer(params.attachment.data, 'binary'),
        filename: params.attachment.filename
      })
    }
    return message
  }


  function fromMail(){
    var from = `${config.fromName} <${config.fromEmail}>`;
    return from;
  }

  function createTemplate(name, content, vars){
    const local = config.templatePath || '/app/mails';
    console.log('process.cwd()', process.cwd());
    let moduleContentTemplate = `${process.cwd()}/${local}/${content}.html`;
    if (!fs.existsSync(moduleContentTemplate)) {
      moduleContentTemplate = `${__dirname}/mails/${content}.html`;
    }
    console.log('moduleContentTemplate:', moduleContentTemplate);
    let moduleNameTemplate = `${process.cwd()}/${local}/${name}.html`;
    if (!fs.existsSync(moduleNameTemplate)) {
      moduleNameTemplate = `${__dirname}/mails/${name}.html`;
    }
    console.log('moduleNameTemplate:', moduleNameTemplate);
    const contentHTML = nunjucks.render(moduleContentTemplate, vars)
    const templateHTML = nunjucks.render(moduleNameTemplate, {
      content: contentHTML,
      logo: config.logo,
      footer: config.footer
    });

    return templateHTML
  }

  function sendMail(params, cb){
    var message = buildMessage(params);

    mailgun.messages().send(message, function (err, response) {
      if(err) return cb(err);

      cb(null, response)
    });
  }

  return {
    send: sendFn
  }
};
